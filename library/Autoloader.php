<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php-autoloader
 * @version   1.0.0
 * @since     2013.11.13.
 */

/**
 * Class Autoloader
 *
 * Class autoloader implementation.
 *
 * @link http://www.php-fig.org/
 */
class Autoloader {
    /**
     * Php file suffix.
     */
    const PHP_FILE_SUFFIX = '.php';

    /**
     * Class instance.
     *
     * @var null|Autoloader
     * @static
     */
    protected static $instance = null;

    /**
     * Include_path configuration option.
     *
     * @var array
     */
    protected $includePath = array();

    /**
     * Constructor
     */
    protected function __construct() {
    }

    /**
     * Returns Autoloader instance.
     *
     * @static
     * @return Autoloader
     */
    public static function getInstance() {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Adds some include path option.
     *
     * @param string|array $includePath
     *
     * @return $this
     */
    public function addIncludePath($includePath) {
        $this->includePath = array_merge($this->includePath, (array)$includePath);

        return $this;
    }

    /**
     * Returns include path options.
     *
     * @return array
     */
    public function getIncludePath() {
        return $this->includePath;
    }


    /**
     * Register loader with SPL autoloader stack.
     *
     * @return $this
     */
    public function register() {
        spl_autoload_register(array(
                                  $this,
                                  'loadClass'
                              ));

        return $this;
    }

    /**
     * Unregister this class loader from the SPL autoloader stack.
     *
     * @return $this
     */
    public function unregister() {
        spl_autoload_unregister(array(
                                    $this,
                                    'loadClass'
                                ));

        return $this;
    }

    /**
     * Loads the given class or interface.
     *
     * @param string $className The name of the class to load.
     *
     * @return $this
     */
    public function loadClass($className) {
        $oldIncludePath = get_include_path();

        set_include_path(implode(PATH_SEPARATOR, $this->getIncludePath()) . PATH_SEPARATOR . $oldIncludePath);

        $fileName = str_replace(array(
                                    '\\',
                                    '_'
                                ),
                                DIRECTORY_SEPARATOR,
                                rtrim($className, '\\')) . self::PHP_FILE_SUFFIX;

        require_once($fileName);

        set_include_path($oldIncludePath);

        return $this;
    }
} 